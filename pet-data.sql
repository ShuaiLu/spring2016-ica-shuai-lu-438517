mysql> create table pets(
    -> id SMALLINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    -> species ENUM('cat','dog','fish','bird','hamster') NOT NULL,
    -> name VARCHAR(50) NOT NULL,
    -> filename VARCHAR(150) NOT NULL,
    -> weight DECIMAL(4,2) NOT NULL,
    -> description TINYTEXT);