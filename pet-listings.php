<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<title>Pet Listings</title>
<style type="text/css">
body{
	width: 760px; /* how wide to make your web page */
	background-color: teal; /* what color to make the background */
	margin: 0 auto;
	padding: 0;
	font:12px/16px Verdana, sans-serif; /* default font */
}
div#main{
	background-color: #FFF;
	margin: 0;
	padding: 10px;
}
</style>
</head>
<body>
 <h1>Pet Listings</h1>   
    <div id="main">
<?php
require 'database.php';
 
$stmt = $mysqli->prepare("select species,count(*) from pets group by species");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}
 
$stmt->execute();
$stmt->bind_result($first, $last);
 
echo "<table><tr><td>species</td><td>number</td></tr>\n";
while($stmt->fetch()){
    echo "<tr>";
	echo "<td>$first</td>";
    echo "<td>$last</td>";
    echo "</tr>";
}
echo "</table>\n";
        
$stmt->close();

$stmt = $mysqli->prepare("select species,name,weight,description,filename from pets ");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->execute();
$stmt->bind_result($species,$name,$weight,$descripton,$filename);
echo "<table><tr><td>species</td><td>number</td></tr>\n";
while($stmt->fetch()){
    echo "<tr>";
    echo "<td>$species</td>";
	echo "<td><b>$name</b></td>";
    echo "<td>$weight</td>";
    echo "<td>$description</td>";
    echo "<td><img src='/var/www/$filename'/></td>";
    echo "</tr>";
}
echo "</table>\n";

 include "add-pet.html";
        
?>
 

 
</div></body>
</html>